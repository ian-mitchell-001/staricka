# The Collected Works of Tanner Staricka

This repository contains the working version (and sources) for the digital book "The Collected Works of Tanner Staricka".

This repository also includes the epigraph.sty file for compiling, for users of lightweight LaTeX systems (such as yours truly).
